﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class HumanSoulPrimeRate : PrimeRate
    {
        private ShoppingCart shoppingCart;

        public HumanSoulPrimeRate(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public double GetPrimeRate()
        {
            for (int counter = 0; counter < this.shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (this.shoppingCart.listOfItemsInsideShoppingCart[counter].ID == "006")
                {
                    return GetHumanSoulPrimeRate(this.shoppingCart.listOfItemsInsideShoppingCart[counter], shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter]);
                }
            }
            return 0;
        }

        private double GetHumanSoulPrimeRate(SoulItem humanSoulItem, int numberOfHumanSoulItems)
        {
            if (shoppingCart.GetOrderSubTotal() > 99.9)
            {
                return ((5 * humanSoulItem.NumberOfSouls)) * numberOfHumanSoulItems;
            }
            return 0;
        }
    }
}
