﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class PrimeRateCalculator
    {
        private List<PrimeRate> PrimeRateList;

        public PrimeRateCalculator(List<PrimeRate> PrimeRateList)
        {
            this.PrimeRateList = PrimeRateList;           
        }

        public double GetPrimeRate()
        {
            double highestPrimeRate = 0;
            for (int i = 0; i < PrimeRateList.Count; i++)
            {
                if (PrimeRateList[i].GetPrimeRate() > highestPrimeRate)
                {
                    highestPrimeRate = PrimeRateList[i].GetPrimeRate();
                }
            }
            return highestPrimeRate;
        }       
    }
}
