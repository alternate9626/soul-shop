﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class DiscountCalculatorFactory
    {
        private ShoppingCart shoppingCart;

        public DiscountCalculatorFactory(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public DiscountCalculator Get()
        {
            List<Discount> listOfDiscounts = new List<Discount>();
            
            GoogleEmailDiscount googleEmailDiscount = new GoogleEmailDiscount(shoppingCart);
            HotmailEmailDiscount hotmailEmailDiscount = new HotmailEmailDiscount(shoppingCart);
            FreeCatAndVladSoulsDiscount freeCatAndVladSoulDiscount = new FreeCatAndVladSoulsDiscount(shoppingCart);

            listOfDiscounts.Add(googleEmailDiscount);
            listOfDiscounts.Add(hotmailEmailDiscount);
            listOfDiscounts.Add(freeCatAndVladSoulDiscount);

            DiscountCalculator discountCalculator = new DiscountCalculator(listOfDiscounts);

            return discountCalculator;
        }
    }
}
