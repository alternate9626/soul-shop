﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class DrakeSoulPrimeRate : PrimeRate
    {
        private ShoppingCart shoppingCart;

        public DrakeSoulPrimeRate(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public double GetPrimeRate()
        {
            for (int counter = 0; counter < this.shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (this.shoppingCart.listOfItemsInsideShoppingCart[counter].ID == "005")
                {
                    return GetDrakeSoulPrimeRate(this.shoppingCart.listOfItemsInsideShoppingCart[counter], shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter]);
                }
            }
            return 0;
        }

        private double GetDrakeSoulPrimeRate(SoulItem drakeSoulItem, int numberOfDrakeSoulItems)
        {
            if (shoppingCart.GetOrderSubTotal() > 124.99)
            {
                return (0.15 * drakeSoulItem.NumberOfSouls) * numberOfDrakeSoulItems;
            }
            return 0;
        }
    }
}
