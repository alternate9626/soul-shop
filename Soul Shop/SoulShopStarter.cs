﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class SoulShopStarter
    {
        private static UserInterface ui = new ConsoleUserInterface();
        private static ListOfItemsToSellFactory listOfItemsToSellGetter = new ListOfItemsToSellFactory();
        private static SoulItem[] listOfItemsToSell = listOfItemsToSellGetter.Get();
        private static ShoppingCart shoppingCart = new ShoppingCart(ui);

        static void Main(string[] args)
        {
            DiscountCalculatorFactory discountCalculatorGetter = new DiscountCalculatorFactory(shoppingCart);
            PrimeRateCalculatorGetter primeRateCalculatorGetter = new PrimeRateCalculatorGetter(shoppingCart);
            Reciept reciept = new Reciept(shoppingCart, primeRateCalculatorGetter.Get(), discountCalculatorGetter.Get());            
            SoulShop soulShop = new SoulShop(ui,listOfItemsToSell,reciept);            
            soulShop.Start();      
        }
        
    }
}
