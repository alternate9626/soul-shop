﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class SoulShop
    {
        private UserInterface ui;       
        private SoulItem[] listOfItemsToSell;        
        private Reciept reciept;
        private String email;

        public SoulShop(UserInterface ui,SoulItem[] listOfItemsToSell, Reciept reciept)
        {
            this.ui = ui;
            this.listOfItemsToSell = listOfItemsToSell;
            this.reciept = reciept;                       
        }

        public void Start()
        {
            ui.ShowWelcomeMessage();
            GetUserEmailAddress();
            MakeAPurchase();    
            while (ui.GetIsUserBuyingAgainBool())
            {
                MakeAPurchase();                
            }            
            ui.ShowAfterPaymentSummary(reciept);
        }

        private void GetUserEmailAddress()
        {
            ui.AskUserEmail();
            this.email = ui.GetUserEmail();
            reciept.DiscountCalculator.SetEmailToDiscounts(this.email);
        }

        private void MakeAPurchase()
        {
            ui.ShowListOfItemsToSell(listOfItemsToSell);
            int userChoice = ui.GetUserChoiceBasedOnNumberOfItemsOnSale(this.listOfItemsToSell.Length);
            SoulItem itemBought = listOfItemsToSell[userChoice - 1];
            reciept.ShoppingCart.Add(itemBought);
            ui.ShowItemAddedToCartMessage(itemBought.ID);    
        }        
    }
}
