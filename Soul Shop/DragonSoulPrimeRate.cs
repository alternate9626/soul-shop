﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class DragonSoulPrimeRate : PrimeRate
    {
        private ShoppingCart shoppingCart;

        public DragonSoulPrimeRate(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public double GetPrimeRate()
        {
            for (int counter = 0; counter < this.shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (this.shoppingCart.listOfItemsInsideShoppingCart[counter].ID == "001")
                {
                    return GetDragonSoulPrimeRate(this.shoppingCart.listOfItemsInsideShoppingCart[counter], shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter]);
                }
            }
            return 0;
        }

        private double GetDragonSoulPrimeRate(SoulItem dragonSoulItem, int numberOfDragonItems)
        {
            if (shoppingCart.GetOrderSubTotal() > 99.9)
            {
                return ((5 * dragonSoulItem.NumberOfSouls)) * numberOfDragonItems;
            }
            return 0;
        }
    }
}
