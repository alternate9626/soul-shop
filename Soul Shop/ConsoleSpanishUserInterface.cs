﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class ConsoleSpanishUserInterface : UserInterface
    {
        public void ShowWelcomeMessage()
        {
            Console.WriteLine();
            Console.WriteLine("Bienvenido a la tienda de almas!");
            Console.WriteLine();
            Console.WriteLine();
        }

        public void AskUserEmail()
        {
            Console.Write("Entre su e-mail:");
        }

        public String GetUserEmail()
        {
            String userEmail = Console.ReadLine();
            Console.Clear();
            return userEmail;
        }

        public void ShowListOfItemsToSell(SoulItem[] listOfItemsToSell)
        {            
            ShowPleaseChooseSoulsToBuyMessage();
            for (int counter = 0; counter < listOfItemsToSell.Length; counter++)
            {
                SoulItem currentItem = listOfItemsToSell[counter];
                Console.WriteLine("#" + (counter + 1) + " - " + GetNameFromID(currentItem.ID) +
                    " <" + currentItem.NumberOfSouls + " almas> - " + currentItem.PriceInDollars + "$");
                Console.WriteLine("");
            }

            Console.WriteLine("----------------------------------------------------");
        }

        private String GetNameFromID(String ID)
        {
            switch (ID)
            {
                case "001":
                    return "Paquete de almas de Dragones";
                case "002":
                    return "Alma de Vlad";
                case "003":
                    return "Almas Gatunas";
                case "004":
                    return "Paquete de almas de Tigres";
                case "005":
                    return "Paquete de almas de Draco";
                case "006":
                    return "Paquete de almas de Humanos";
                case "007":
                    return "Paquete de almas Angelicales";
                default:
                    return "Unamed Item";
            }
        }

        private void ShowPleaseChooseSoulsToBuyMessage()
        {
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("Elije que almas comprar");
            Console.WriteLine("-----------------------------------------------------");
        }

        public int GetUserChoiceBasedOnNumberOfItemsOnSale(int numberOfItemsOnSale)
        {
            Console.Write("Alma a comprar: ");
            int userChoice = int.Parse(Console.ReadLine());
            while (IsChoiceInvalid(userChoice, numberOfItemsOnSale))
            {
                ShowInvalidChoiceMessage();
                userChoice = int.Parse(Console.ReadLine());
            }
            Console.Clear();
            return userChoice;
        }

        private bool IsChoiceInvalid(int userChoice, int numberOfItemsOnSale)
        {
            return userChoice > numberOfItemsOnSale || userChoice < 1;
        }

        public void ShowInvalidChoiceMessage()
        {
            Console.WriteLine("Opcion no disponible. Intente de nuevo:");
        }

        public bool GetIsUserBuyingAgainBool()
        {
            IsUserBuyingAgainMessage();
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("1)Si 2)No");
            Console.WriteLine("Eleccion: ");
            int userChoice = int.Parse(Console.ReadLine());
            Console.Clear();
            return userChoice == 1;
        }

        public void ShowItemAddedToCartMessage(String itemID)
        {
            Console.WriteLine(GetNameFromID(itemID) + " fue anadido a su carrito de compra!");
        }

        private void IsUserBuyingAgainMessage()
        {
            Console.WriteLine("desea comprar mas almas?");
        }

        public void ShowOrderSubTotal(double subTotal)
        {
            Console.WriteLine("SubTotal: " + subTotal);
        }

        public void ShowOrderTaxes(double orderTaxes)
        {
            Console.WriteLine("Taxes: " + orderTaxes);
        }

        public void ShowOrderTotal(double orderTotal)
        {
            Console.WriteLine("Total: " + orderTotal);
        }

        public void ShowOrderPrimeRate(double primeRate)
        {
            Console.WriteLine("Prime Rate: " + primeRate);
        }

        public void ShowThanksForBuyingMessage()
        {
            Console.WriteLine("Gracias por preferirnos! presiona cualquier tecla, desues presiona Enter para salir. :D");
            Console.ReadLine();
        }

        public void ShowOrderSummary(List<SoulItem> listOfItemsInsideShoppingCart, List<int> listOfQuantitiesInsideShoppingCart)
        {
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Resumen de cuenta:");
            Console.WriteLine("----------------------------------------------------");
            for (int counter = 0; counter < listOfItemsInsideShoppingCart.Count(); counter++)
            {
                Console.WriteLine(listOfQuantitiesInsideShoppingCart[counter] + "x " + GetNameFromID(listOfItemsInsideShoppingCart[counter].ID) + " -- " + listOfItemsInsideShoppingCart[counter].NumberOfSouls + " souls -- " +
                (listOfItemsInsideShoppingCart[counter].PriceInDollars * listOfQuantitiesInsideShoppingCart[counter]) + "$");
            }

            Console.WriteLine("----------------------------------------------------");
        }

        public void ShowOrderDiscount(double discount)
        {
            Console.WriteLine("Descuento: " + discount);
        }

        public void ShowAfterPaymentSummary(Reciept reciept)
        {
            double orderTotal = reciept.GetOrderTotal();
            ShowOrderSummary(reciept.ShoppingCart.listOfItemsInsideShoppingCart, reciept.ShoppingCart.listOfQuantitiesOfItemsInsideShoppingCart);
            ShowOrderSubTotal(reciept.GetOrderSubTotal());
            ShowOrderPrimeRate(reciept.GetTotalPrimeRate());
            ShowOrderDiscount(reciept.GetTotalDiscount());
            ShowOrderTaxes(reciept.GetTaxes());
            ShowOrderTotal(orderTotal);
            ShowThanksForBuyingMessage();
        }
    }
}
