﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class PrimeRateCalculatorGetter
    {
        private ShoppingCart shoppingCart;

        public PrimeRateCalculatorGetter (ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public PrimeRateCalculator Get()
        {
            List<PrimeRate> listOfPrimeRates = new List<PrimeRate>();

            CatSoulPrimeRate CatSoulPrimeRate = new CatSoulPrimeRate(shoppingCart);
            DragonSoulPrimeRate DragonSoulPrimeRate = new DragonSoulPrimeRate(shoppingCart);
            DrakeSoulPrimeRate DrakeSoulPrimeRate = new DrakeSoulPrimeRate(shoppingCart);
            HumanSoulPrimeRate HumanSoulPrimeRate = new HumanSoulPrimeRate(shoppingCart);
            TigerSoulPrimeRate TigerSoulPrimeRate = new TigerSoulPrimeRate(shoppingCart);
            AngelSoulPrimeRate AngelSoulPrimeRate = new AngelSoulPrimeRate(shoppingCart);
            VladSoulPrimeRate VladSoulPrimeRate = new VladSoulPrimeRate(shoppingCart);

            listOfPrimeRates.Add(CatSoulPrimeRate);
            listOfPrimeRates.Add(DragonSoulPrimeRate);
            listOfPrimeRates.Add(DrakeSoulPrimeRate);
            listOfPrimeRates.Add(HumanSoulPrimeRate);
            listOfPrimeRates.Add(TigerSoulPrimeRate);
            listOfPrimeRates.Add(AngelSoulPrimeRate);
            listOfPrimeRates.Add(VladSoulPrimeRate);

            PrimeRateCalculator primeRateCalculator = new PrimeRateCalculator(listOfPrimeRates);

            return primeRateCalculator;
        }
    }
}
