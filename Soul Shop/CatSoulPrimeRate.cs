﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class CatSoulPrimeRate : PrimeRate
    {
        private ShoppingCart shoppingCart;

        public CatSoulPrimeRate(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public double GetPrimeRate()
        {
            for (int counter = 0; counter < this.shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (this.shoppingCart.listOfItemsInsideShoppingCart[counter].ID == "003")
                {
                    return GetCatSoulPrimeRate(this.shoppingCart.listOfItemsInsideShoppingCart[counter], shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter]);
                }                
            }
            return 0;
        }

        private double GetCatSoulPrimeRate(SoulItem catItem, int numberOfCatItems)
        {
            return ((0.25 * catItem.PriceInDollars) * numberOfCatItems);
        }
    }
}
