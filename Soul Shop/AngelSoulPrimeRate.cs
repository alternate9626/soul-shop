﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class AngelSoulPrimeRate : PrimeRate
    {
        private ShoppingCart shoppingCart;

        public AngelSoulPrimeRate(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public double GetPrimeRate()
        {
            for (int counter = 0; counter < this.shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (this.shoppingCart.listOfItemsInsideShoppingCart[counter].ID == "007")
                {
                    return GetAngelSoulPrimeRate(this.shoppingCart.listOfItemsInsideShoppingCart[counter], shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter]);
                }
            }
            return 0;
        }

        private double GetAngelSoulPrimeRate(SoulItem angelSoulItem, int numberOfAngelSoulItems)
        {
            if (shoppingCart.GetOrderSubTotal() > 199.99)
            {
                return (15 * angelSoulItem.NumberOfSouls) * numberOfAngelSoulItems;
            }
            return 0;
        }
    }
}
