﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    interface UserInterface
    {
        void ShowWelcomeMessage();
        void AskUserEmail();
        String GetUserEmail();
        void ShowListOfItemsToSell(SoulItem[] listOfItemsToSell);
        int GetUserChoiceBasedOnNumberOfItemsOnSale(int numberOfItemsOnSale);
        void ShowInvalidChoiceMessage();
        bool GetIsUserBuyingAgainBool();
        void ShowItemAddedToCartMessage(String itemID);        
        void ShowOrderSubTotal(double subTotal);
        void ShowOrderPrimeRate(double discount);
        void ShowOrderTaxes(double orderTaxes);
        void ShowOrderTotal(double orderTotal);
        void ShowThanksForBuyingMessage();
        void ShowOrderSummary(List<SoulItem> listOfItemsInsideShoppingCart,List<int> listOfQuantitiesInsideShoppingCart);
        void ShowAfterPaymentSummary(Reciept reciept);        
    }
}
