﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class SoulItem
    {
        public String ID { private set; get; }        
        public int NumberOfSouls { private set; get; }
        public double PriceInDollars { private set; get; }       

        public SoulItem(String ID, int numberOfSouls, int priceInDollars)
        {
            this.ID = ID;            
            this.NumberOfSouls = numberOfSouls;
            this.PriceInDollars = priceInDollars;
        }     
    }
}
