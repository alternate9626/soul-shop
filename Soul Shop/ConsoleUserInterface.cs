﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class ConsoleUserInterface : UserInterface 
    {
        public void ShowWelcomeMessage()
        {
            Console.WriteLine();
            Console.WriteLine("Welcome to Vlad's Soul Shop! 66.6% OFF on particular souls! ;)");
            Console.WriteLine();
            Console.WriteLine();
        }

        public void AskUserEmail()
        {
            Console.Write("Please enter your E-mail address:");
        }

        public String GetUserEmail()
        {
            String userEmail = Console.ReadLine();
            Console.Clear();
            return userEmail;        
        }

        public void ShowListOfItemsToSell(SoulItem[] listOfItemsToSell)
        {
            ShowPleaseChooseSoulsToBuyMessage();
            for (int counter = 0; counter < listOfItemsToSell.Length; counter++)
            {
                SoulItem currentItem = listOfItemsToSell[counter];
                Console.WriteLine("#" + (counter + 1) + " - " + GetItemNameFromID(currentItem.ID) + 
                    " <" + currentItem.NumberOfSouls + " souls> - " + currentItem.PriceInDollars + "$");
                Console.WriteLine("");
            }

            Console.WriteLine("----------------------------------------------------");
        }

        private String GetItemNameFromID(String ID)
        {
            switch (ID)
            {
                case "001":
                    return "Dragon Soul Pack";
                case "002":
                    return "Vlad's Soul";
                case "003":
                    return "Cat's Souls";
                case "004":
                    return "Tiger Soul Pack";
                case "005":
                    return "Drake Soul Pack";
                case "006":
                    return "Human Soul Pack";
                case "007":
                    return "Angel Soul Pack";
                default:
                    return "Unamed Item";
            }
        }

        private void ShowPleaseChooseSoulsToBuyMessage()
        {
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("Please choose souls to buy");
            Console.WriteLine("-----------------------------------------------------");
        }

        public int GetUserChoiceBasedOnNumberOfItemsOnSale(int numberOfItemsOnSale)
        {
            Console.Write("Enter Choice: ");
            int userChoice = int.Parse(Console.ReadLine());
            while (IsChoiceInvalid(userChoice,numberOfItemsOnSale))
            {
                ShowInvalidChoiceMessage();
                userChoice = int.Parse(Console.ReadLine());
            }
            Console.Clear();
            return userChoice;
        }

        private bool IsChoiceInvalid(int userChoice,int numberOfItemsOnSale)
        {
            return userChoice > numberOfItemsOnSale|| userChoice < 1;
        }

        public void ShowInvalidChoiceMessage()
        {
            Console.WriteLine("Wrong choice. Try again.");
        }

        public bool GetIsUserBuyingAgainBool()
        {
            IsUserBuyingAgainMessage();
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("1)Yes 2)No");
            Console.WriteLine("Enter Choice: ");
            int userChoice = int.Parse(Console.ReadLine());
            Console.Clear();
            return userChoice == 1;
        }

        public void ShowItemAddedToCartMessage(String itemID)
        {
            Console.WriteLine(GetItemNameFromID(itemID) + " was added to your shopping cart!");
        }

        private void IsUserBuyingAgainMessage()
        {
            Console.WriteLine("Do you want to buy another item?");
        }

        public void ShowOrderSubTotal(double subTotal)
        {
            Console.WriteLine("SubTotal: " + subTotal);
        }

        public void ShowOrderTaxes(double orderTaxes)
        {
            Console.WriteLine("Taxes: " + orderTaxes);
        }

        public void ShowOrderTotal(double orderTotal)
        {
            Console.WriteLine("OrderTotal: " + orderTotal);
        }

        public void ShowOrderPrimeRate(double primeRate)
        {
            Console.WriteLine("Prime Rate: " + primeRate);
        }

        public void ShowThanksForBuyingMessage()
        {
            Console.WriteLine("Thanks for buying today! Press any key then enter to exit :D");
            Console.ReadLine();
        }

        public void ShowOrderSummary(List<SoulItem> listOfItemsInsideShoppingCart, List<int> listOfQuantitiesInsideShoppingCart)
        {
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Order Summary:");
            Console.WriteLine("----------------------------------------------------");
            for (int counter = 0; counter < listOfItemsInsideShoppingCart.Count(); counter++)
            {
                Console.WriteLine(listOfQuantitiesInsideShoppingCart[counter] + "x " + GetItemNameFromID(listOfItemsInsideShoppingCart[counter].ID) + " -- " + listOfItemsInsideShoppingCart[counter].NumberOfSouls + " souls -- " +
                (listOfItemsInsideShoppingCart[counter].PriceInDollars * listOfQuantitiesInsideShoppingCart[counter]) + "$");
            }

            Console.WriteLine("----------------------------------------------------");
        }

        public void ShowOrderDiscount(double discount)
        {
            Console.WriteLine("Discount: " + discount);
        }

        public void ShowAfterPaymentSummary(Reciept reciept)
        {           
            double orderTotal = reciept.GetOrderTotal();
            ShowOrderSummary(reciept.ShoppingCart.listOfItemsInsideShoppingCart, reciept.ShoppingCart.listOfQuantitiesOfItemsInsideShoppingCart);
            ShowOrderSubTotal(reciept.GetOrderSubTotal());
            ShowOrderPrimeRate(reciept.GetTotalPrimeRate());            
            ShowOrderDiscount(reciept.GetTotalDiscount());
            ShowOrderTaxes(reciept.GetTaxes());            
            ShowOrderTotal(orderTotal);   
           ShowThanksForBuyingMessage();
        }        
    }
}
