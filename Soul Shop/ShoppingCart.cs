﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class ShoppingCart
    {
        private UserInterface ui;
        public List<SoulItem> listOfItemsInsideShoppingCart { private set; get; }
        public List<int> listOfQuantitiesOfItemsInsideShoppingCart { private set; get; }

        public ShoppingCart(UserInterface ui)
        {
            this.ui = ui;
            this.listOfItemsInsideShoppingCart = new List<SoulItem>();
            this.listOfQuantitiesOfItemsInsideShoppingCart = new List<int>();    
        }

        public void Add(SoulItem soulItemToBeAdded)
        {
            if (!listOfItemsInsideShoppingCart.Contains(soulItemToBeAdded))
            {
                listOfItemsInsideShoppingCart.Add(soulItemToBeAdded);
                listOfQuantitiesOfItemsInsideShoppingCart.Add(1);
            }
            else
            {
                AddARepeatedItem(soulItemToBeAdded);
            }                                        
        }

        private void AddARepeatedItem(SoulItem soulItemToBeAdded)
        {
            for (int counter = 0; counter < listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (listOfItemsInsideShoppingCart[counter].ID == soulItemToBeAdded.ID)
                {
                    listOfQuantitiesOfItemsInsideShoppingCart[counter]++;
                }
            }
        }

        public double GetOrderSubTotal()
        {
            double orderSubTotal = 0;
            for (int counter = 0; counter < listOfItemsInsideShoppingCart.Count(); counter++)
            {
                orderSubTotal += listOfItemsInsideShoppingCart[counter].PriceInDollars * listOfQuantitiesOfItemsInsideShoppingCart[counter];
            }
            return orderSubTotal;
        }
    }
}
