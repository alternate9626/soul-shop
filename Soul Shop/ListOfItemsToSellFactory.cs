﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class ListOfItemsToSellFactory
    {
        public SoulItem[] Get()
        {
            return GetSoulItems();
        }

        private SoulItem[] GetSoulItems()
        {
            SoulItem[] soulItemsList = new SoulItem[7];
            soulItemsList[0] = GetDragonSoulPackItem();
            soulItemsList[1] = GetVladSoulItem();
            soulItemsList[2] = GetCatSoulPackItem();
            soulItemsList[3] = GetTigerSoulPackItem();
            soulItemsList[4] = GetHumanSoulPackItem();
            soulItemsList[5] = GetDrakeSoulPackItem();
            soulItemsList[6] = GetAngelSoulPackItem();
            return soulItemsList;
        }

        private SoulItem GetDragonSoulPackItem()
        {
            return new SoulItem("001", 5, 100);            
        }

        private SoulItem GetVladSoulItem()
        {
            return new SoulItem("002", 1, 1);            
        }

        private SoulItem GetCatSoulPackItem()
        {
            return new SoulItem("003", 3, 25);            
        }

        private SoulItem GetTigerSoulPackItem()
        {
           return new SoulItem("004", 4, 57);            
        }

        private SoulItem GetDrakeSoulPackItem()
        {
            return new SoulItem("005", 10, 80);
        }

        private SoulItem GetHumanSoulPackItem()
        {
            return new SoulItem("006", 8, 20);
        }

        private SoulItem GetAngelSoulPackItem()
        {
            return new SoulItem("007", 3, 120);
        }
    }
}
