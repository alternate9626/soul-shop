﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class DiscountCalculator
    {
        public List<Discount> ListOfDiscounts;
        
        public DiscountCalculator(List<Discount> ListOfDiscounts)
        {
            this.ListOfDiscounts = ListOfDiscounts;
        }

        public void SetEmailToDiscounts(String email)
        {
            for (int i = 0; i < ListOfDiscounts.Count(); i++)
            {
                ListOfDiscounts[i].SetEmail(email);
            }
        }

        public double GetTotalDiscount()
        {
            double totalDiscount = 0;            
            for (int i = 0; i < ListOfDiscounts.Count(); i++)
            {
                totalDiscount += ListOfDiscounts[i].GetDiscount();
            }
            return totalDiscount;  
        }
    }
}

