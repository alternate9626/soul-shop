﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class HotmailEmailDiscount : Discount
    {
        
        private ShoppingCart shoppingCart;
        public String email;

        public HotmailEmailDiscount(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public void SetEmail(String email)
        {
            this.email = email;
        }

        public double GetDiscount()
        {
            if (email.Contains("hotmail.com") && shoppingCart.GetOrderSubTotal() > 199.99)
            {
                return GetHotmailEmailDiscount();
            }

            return 0.00;
        }

        private double GetHotmailEmailDiscount()
        {
            double discount = 0;
            for (int counter = 0; counter < shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (shoppingCart.listOfItemsInsideShoppingCart[counter].ID == "003" || shoppingCart.listOfItemsInsideShoppingCart[counter].ID == "004")
                {
                    discount += (0.50 * shoppingCart.listOfItemsInsideShoppingCart[counter].PriceInDollars) * shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter];
                }
            }
            return discount;
        }
    }
}
