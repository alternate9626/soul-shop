﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class FreeCatAndVladSoulsDiscount : Discount
    {
        private ShoppingCart shoppingCart;
        private String email;

        public FreeCatAndVladSoulsDiscount(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public void SetEmail(String email)
        {
            this.email = email;
        }

        public double GetDiscount()
        {
            if (IsBuyingXAmountOfYItem(200, "004"))
            {
                ApplyFreeCatAndVladSouls();
            }
            return 0.00;
        }

        private bool IsBuyingXAmountOfYItem(int amountOfItems, String itemID)
        {
            for (int counter = 0; counter < shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (shoppingCart.listOfItemsInsideShoppingCart[counter].ID == itemID)
                {
                    if ((shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter] * shoppingCart.listOfItemsInsideShoppingCart[counter].NumberOfSouls) > (amountOfItems - 1))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void ApplyFreeCatAndVladSouls()
        {
            SoulItem free20SoulCatItem = new SoulItem("003", 20, 0);
            SoulItem freeVladSoul = new SoulItem("002", 1, 0);
            shoppingCart.Add(free20SoulCatItem);
            shoppingCart.Add(freeVladSoul);
        }
    }
}
