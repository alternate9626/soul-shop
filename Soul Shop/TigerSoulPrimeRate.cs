﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class TigerSoulPrimeRate : PrimeRate
    {
        private ShoppingCart shoppingCart;

        public TigerSoulPrimeRate(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public double GetPrimeRate()
        {
            for (int counter = 0; counter < this.shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                if (this.shoppingCart.listOfItemsInsideShoppingCart[counter].ID == "004")
                {
                    return GetTigerSoulPrimeRate(this.shoppingCart.listOfItemsInsideShoppingCart[counter], shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter]);
                }                
            }
            return 0;
        }

        private double GetTigerSoulPrimeRate(SoulItem tigerItem, int numberOfTigerItems)
        {
            if (shoppingCart.GetOrderSubTotal() > 99.9)
            {
                return ((5 * tigerItem.NumberOfSouls)) * numberOfTigerItems;
            }
            else
            {
                return 10;
            }
        }
    }
}
