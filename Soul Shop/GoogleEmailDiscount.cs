﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class GoogleEmailDiscount : Discount
    {
        private double emailGoogleDiscountInPercentage = 0.30;
        private ShoppingCart shoppingCart;
        public String email;

        public GoogleEmailDiscount(ShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        public void SetEmail(String email)
        {
            this.email = email;
        }

        public double GetDiscount()
        {
            if (email.Contains("gmail.com"))
            {
                return GetGoogleEmailDiscount();
            }           

            return 0.00;
        }

        private double GetGoogleEmailDiscount()
        {
            double discount = 0;
            for (int counter = 0; counter < shoppingCart.listOfItemsInsideShoppingCart.Count; counter++)
            {
                discount += ((emailGoogleDiscountInPercentage * shoppingCart.listOfItemsInsideShoppingCart[counter].PriceInDollars) * shoppingCart.listOfQuantitiesOfItemsInsideShoppingCart[counter]);
            }
            return discount;
        }
    }
}
