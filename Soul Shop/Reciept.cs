﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soul_Shop
{
    class Reciept
    {
        private double taxesInPercentage = 0.06;
        public ShoppingCart ShoppingCart { private set; get; }
        public PrimeRateCalculator PrimeRateCalculator { private set; get; }
        public DiscountCalculator DiscountCalculator { private set; get; }        

        public Reciept(ShoppingCart shoppingCart,PrimeRateCalculator primeRateCalculator,DiscountCalculator discountCalculator)
        {
            this.ShoppingCart = shoppingCart;
            this.PrimeRateCalculator = primeRateCalculator;
            this.DiscountCalculator = discountCalculator;          
        }

        public double GetTaxes()
        {
            return taxesInPercentage * GetOrderSubTotal();
        }

        public double GetOrderTotal()
        {          
            return GetOrderSubTotal() + GetTaxes() - GetTotalPrimeRate() - GetTotalDiscount();
        } 

        public double GetOrderSubTotal()
        {
            return ShoppingCart.GetOrderSubTotal();
        }
        public double GetTotalPrimeRate()
        {
            return PrimeRateCalculator.GetPrimeRate();
        }

        public double GetTotalDiscount()
        {
            return DiscountCalculator.GetTotalDiscount();
        }


    }
}
